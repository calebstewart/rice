# Caleb Stewart's RICE Setup

These are my configuration files and scripts for Arch Linux.

Some of the setup and configuration is based on the VoidRice repo by Luke Smith.
This layout of this repo is compatible with the LARBS installer. You can do this
to install with my configuration:

```sh
git clone https://gitlab.com/calebstewart/rice.git
curl -LO https://larbs.xyz/larbs.sh
chmod +x larbs.sh
./larbs.sh -r /root/rice -p /root/rice/packages.csv -a yay
```

