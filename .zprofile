#!/bin/zsh

# Took this from Luke Smith VoidRice. Cool one-liner to combine
# all the directories in `~/.local/bin`.
export PATH="$PATH:$(du "$HOME/.local/bin" | cut -f2 | paste -sd ':')"
export PATH="$(ruby -r rubygems -e 'puts Gem.user_dir')/bin:$PATH"

# Perl setup (unsure if I actually need this; I'd like to move it)
export PATH="/home/caleb/perl5/bin${PATH:+:${PATH}}"
export PERL5LIB="/home/caleb/perl5/lib/perl5${PERL5LIB:+:${PERL5LIB}}"
export PERL_LOCAL_LIB_ROOT="/home/caleb/perl5${PERL_LOCAL_LIB_ROOT:+:${PERL_LOCAL_LIB_ROOT}}"
export PERL_MB_OPT="--install_base \"/home/caleb/perl5\""
export PERL_MM_OPT="INSTALL_BASE=/home/caleb/perl5"

# Preferred applications
export EDITOR="nvim"
export TERMINAL="st"
export BROWSER="firefox"

# Put things outside of home
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CACHE_HOME="$HOME/.cache"
export GTK2_RC_FILES="${XDG_CONFIG_HOME}/gtk-2.0/gtkrc-2.0"
export LESSHISTFILE="-"
export ZDOTDIR="${XDG_CONFIG_HOME}/zsh"
export WINEPREFIX="${XDG_DATA_HOME}/wineprefixes/default"
export PASSWORD_STORE_DIR="${XDG_DATA_HOME}/password-store"
export TMUX_TMPDIR="$XDG_RUNTIME_DIR"
export CARGO_HOME="$XDG_DATA_HOME/cargo"
export GOPATH="$XDG_CONFIG_HOME/go"
export ANSIBLE_CONFIG="$XDG_CONFIG_HOME/ansible/ansible.cfg"
export HISTFILE="$XDG_DATA_HOME/zsh_history"
export HISTSIZE=10000
export SAVEHIST=10000

# virsh default connection is system not user!
export LIBVIRT_DEFAULT_URI=qemu:///system

# Start X if we are on tty1
[ "$(tty)" = "/dev/tty1" ] && ! pidof Xorg >/dev/null 2>&1 && exec startx
