#!/bin/bash

options=$(cat <<END
🔒 lock	slock
🚪 leave dwm	kill -TERM $(pidof -s dwm)
♻ renew dwm	kill -HUP $(pidof -s dwm)
🔃 reboot	systemctl reboot
🖥 shutdown	systemctl poweroff
END
)

choice=$(echo "$options" | cut -d'	' -f1 | dmenu) || exit 1

`echo "$options" | grep "^$choice	" | cut -d '	' -f2-`
