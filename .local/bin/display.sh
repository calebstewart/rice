#!/usr/bin/env bash

choices="internal\nclone\nhdmi\ndual\nhome"

choice=$(echo -e "$choices" | dmenu -i)

contains() {
	[[ $1 =~ (^|[[:space:]])$2($|[[:space:]]) ]] && return 0 || return 1
}

xrandr_except() {
	for display in $(xrandr -q | grep -i " connected" | awk '{ print $1 }'); do
		contains "$1" "$display" || xrandr --output "$display" ${@:2}
	done
}

case "$choice" in
	internal) xrandr_except "eDP-1" --off; xrandr --output "eDP-1" --mode 1920x1080 ;;
	clone) xrandr --output "eDP-1" --mode 1920x1080; xrandr_except "eDP-1" --same-as eDP-1 ;;
	hdmi) xrandr_except "HDMI-1" --off; xrandr --output "HDMI-1" --mode 1920x1080 ;;
	dual) xrandr_except "HDMI-1 eDP-1" --off; xrandr --output "eDP-1" --mode 1920x1080 --output "HDMI-1" --mode 1920x1080 --right-of eDP-1 ;;
	home) xrandr --output eDP-1 --off --output HDMI-1 --mode 1920x1080 --output DP-2-1 --right-of HDMI-1 --mode 1920x1080 --output DP-2-2 --left-of HDMI-1 --mode 1920x1080
esac
